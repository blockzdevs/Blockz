#ifndef BLOCKZ_OBSERVERTEST_H
#define BLOCKZ_OBSERVERTEST_H

#include <QObject>
#include "../Subject.h"
#include "../Observer.h"

class ObserverTest : public QObject {
    Q_OBJECT
private slots:
    void init() {
        subject.removeObserver(&observer);
        observer.setSubject(nullptr);
    }

    void observer_notify();

    void observer_add_remove();

private:
    class TestSubject : public Subject {
    public:
        int getData() const {
            return data;
        }

        void setData(int data) {
            TestSubject::data = data;
            notify();
        }

    private:
        int data = 0;
    };

    class MockObserver : public Observer {
    public:
        void setSubject(TestSubject* subject) {
            this->subject = subject;

            if (subject) {
                this->subject->registerObserver(this);
            }
        }

        //MOCK_METHOD0(update, void());
        void update() override {}

    private:
        TestSubject* subject = nullptr;
    };

protected:
    MockObserver observer;
    TestSubject subject;
};
#endif //BLOCKZ_OBSERVERTEST_H
