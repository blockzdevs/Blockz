#include "WaveformWidget.h"
#include <QPainter>
#include <QEvent>
#include <QHoverEvent>
#include <cmath>
#include <iostream>
#include <random>

WaveformWidget::WaveformWidget(QString path, QColor color, QWidget* parent) :
        WaveformWidget{path, generateDataFromPath(path), color, parent} {}

WaveformWidget::WaveformWidget(QString path, std::vector<unsigned int> data, QColor color, QWidget* parent) :
        QWidget{parent},
        path{std::move(path)},
        data{std::move(data)},
        container{QPoint{0, 0}, QSize{50, 40}},
        bars{},
        color{std::move(color)},
        barWidth{0.0},
        position{0},
        value{0} {
    setMinimumSize(50, 40);
}

void WaveformWidget::paintEvent(QPaintEvent* event) {
    container.setWidth(width());
    bars.clear();

    QPainter painter{this};
    painter.setRenderHint(QPainter::Antialiasing);

    QPointF startPoint{container.topLeft()};
    barWidth = static_cast<double>(width()) / data.size();
    int height{container.height()};

    auto minMaxIterators = std::minmax_element(data.begin(), data.end());

    if (minMaxIterators.first == data.end() || minMaxIterators.second == data.end()) {
        painter.drawRect(container);
        return;
    }

    unsigned int minHeight = *minMaxIterators.first;
    unsigned int maxHeight = *minMaxIterators.second;

    double delta{static_cast<double>(maxHeight) - static_cast<double>(minHeight)};

    for (unsigned int barHeight : data) {

        // normalize bar height (see: https://stats.stackexchange.com/questions/70801/how-to-normalize-data-to-0-1-range#70807)
        double normalizedHeight{height * (static_cast<double>(barHeight - minHeight) / delta)};

        QPointF topLeft{startPoint.x(), startPoint.y() + (height - normalizedHeight)};
        QRectF bar{topLeft, QSizeF{barWidth, normalizedHeight}};
        bars.push_back(bar);
        startPoint.setX(startPoint.x() + barWidth);
    }

    painter.setPen(QPen{Qt::NoPen});

    painter.setBrush(QBrush{QColor{255, 255, 255, 180}});
    painter.drawRects(bars.data() + value, bars.size() - value);

    painter.setBrush(QBrush{color});
    painter.drawRects(bars.data(), value);
}

void WaveformWidget::setMaximum(unsigned long maximum) {
    this->maximum = maximum;
}

void WaveformWidget::setSliderPosition(int pos) {
    position = pos;
    value = static_cast<int>(std::ceil((static_cast<double>(position) / maximum) * data.size()));
    update();
}

void WaveformWidget::mousePressEvent(QMouseEvent* event) {
    updateValue(event->localPos().x());
    emit valueChanged(static_cast<int>(std::ceil((static_cast<double>(value) / data.size()) * maximum)));
    update();
    QWidget::mousePressEvent(event);
}

void WaveformWidget::mouseMoveEvent(QMouseEvent* event) {
    updateValue(event->localPos().x());
    emit valueChanged(static_cast<int>(std::ceil((static_cast<double>(value) / data.size()) * maximum)));
    update();
    QWidget::mouseMoveEvent(event);
}

std::vector<unsigned int> WaveformWidget::generateDataFromPath(QString path, int size) {
    std::size_t hash = std::hash<std::string>{}(path.toStdString());
    std::mt19937 rng{static_cast<std::mt19937::result_type>(hash)};
    std::uniform_int_distribution<int> dist(0, 300);
    std::vector<unsigned int> ret(static_cast<unsigned long>(size));

    auto fill = std::bind(dist, rng);
    std::generate(ret.begin(), ret.end(), fill);
    std::shuffle(ret.begin(), ret.end(), rng);

    return ret;
}

void WaveformWidget::updateValue(double xCoord) {
    QVector<QRectF>::iterator lastBarIt{std::find_if(bars.begin(), bars.end(), [xCoord](const QRectF& rect) -> bool {
        return rect.bottomRight().x() > xCoord;
    })};

    if (lastBarIt != bars.end()) {
        if (bars.last().bottomLeft().x() < xCoord) {
            // special handle case of hover on last bar
            value = bars.size();
        } else {
            value = static_cast<int>(std::distance(bars.begin(), lastBarIt));
        }
    } else {
        value = static_cast<int>(data.size());
    }
}

void WaveformWidget::setColor(const QColor& color) {
    WaveformWidget::color = color;
}
