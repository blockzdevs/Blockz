#include <memory>
#include "Song.h"

using std::string;

Song::Song(int bpm, Signature signature, Properties properties) :
        bpm{bpm},
        signature{signature},
        properties{properties} {}

int Song::getBpm() const {
    return bpm;
}

void Song::setBpm(int bpm) {
    Song::bpm = bpm;
    notify();
}

const Signature& Song::getSignature() const {
    return signature;
}

void Song::setSignature(const Signature& signature) {
    Song::signature = signature;
    notify();
}

const std::list<std::unique_ptr<Track>>& Song::getTracks() const {
    return tracks;
}

void Song::addTrack(std::unique_ptr<Track> track) {
    tracks.push_back(std::move(track));
    notify();
}

void Song::removeTrack(const Track* track) {
    tracks.remove_if([track](const std::unique_ptr<Track>& el) {
        return el.get() == track;
    });

    notify();
}










