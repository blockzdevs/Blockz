#ifndef BLOCKZ_EDITBLOCKACTION_H
#define BLOCKZ_EDITBLOCKACTION_H

#include "AddBlockDialog.h"

class EditBlockDialog : public AddBlockDialog {
Q_OBJECT
public:
    EditBlockDialog(Track* track, Block* block, QWidget* parent = nullptr);

public slots:
    void on_buttonBox_accepted() override;

signals:
    void onBlockEdit();

private:
    Block* block;
    Track* track;
};


#endif //BLOCKZ_EDITBLOCKACTION_H
