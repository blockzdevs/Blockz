#ifndef BLOCKZ_DASHBOARDCONTROLLER_H
#define BLOCKZ_DASHBOARDCONTROLLER_H

#include <blockz/Song.h>
#include <blockz/PlaybackStatus.h>

class DashboardView;

class DashboardController : public QObject {
Q_OBJECT

    std::unique_ptr<Song> song;
public:
    explicit DashboardController(DashboardView* view);

    void setSong(std::unique_ptr<Song> song);

    Song* getSong();

    void addTrack(std::unique_ptr<Track> track);

    void removeTrack(Track* track);

    const std::list<std::unique_ptr<Track>>& getTracks();

    void restart();

    void setMuteTrack(std::unique_ptr<Track> track);

public slots:
    void play();
    void pause();
    void stop();

private:
    DashboardView* view;
};


#endif //BLOCKZ_DASHBOARDCONTROLLER_H
