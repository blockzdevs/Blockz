#include <QTest>
#include "SongTest.h"
#include "ObserverTest.h"

#define Q_TEST(test_case) test_case __##test_case; \
        status |= QTest::qExec(&__##test_case, argc, argv)

int main(int argc, char** argv) {
    QCoreApplication app{argc, argv};
    int status = 0;

    Q_TEST(ObserverTest);
    Q_TEST(SongTest);

    return status;
}