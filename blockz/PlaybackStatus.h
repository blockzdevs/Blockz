#ifndef BLOCKZ_PLAYBACKSTATUS_H
#define BLOCKZ_PLAYBACKSTATUS_H

enum class PlaybackStatus {
    Playing,
    Paused,
    Stopped
};

#endif //BLOCKZ_PLAYBACKSTATUS_H
