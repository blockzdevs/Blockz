#ifndef BLOCKZ_SONGTEST_H_H
#define BLOCKZ_SONGTEST_H_H

#include <QObject>
#include "../Song.h"

class SongTest : public QObject {
    Q_OBJECT

private slots:
    void init() {
        song = Song();
    }

    void song_track_add();

    void song_track_remove();

    void song_props_get_set();

private:
    Song song;
};

#endif //BLOCKZ_SONGTEST_H_H
