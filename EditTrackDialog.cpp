#include "EditTrackDialog.h"

EditTrackDialog::EditTrackDialog(Track* track, QWidget* parent) :
        AddTrackDialog{parent},
        track{track} {
    ui.nameInput->setText(QString::fromStdString(track->getProperties().getName()));

    int index{ui.colorInput->findData(QVariant::fromValue(track->getProperties().getColor()))};
    if (index == -1) {
        qDebug() << Q_FUNC_INFO << "Invalid color";
        return;
    }

    ui.colorInput->setCurrentIndex(index);
}

void EditTrackDialog::on_buttonBox_accepted() {
    float volume{static_cast<float>(ui.volumeSlider->value())};
    Color color{ui.colorInput->currentData().value<Color>()};
    QString name{ui.nameInput->text()};

    Properties& properties{track->getProperties()};
    properties.setVolume(volume);
    properties.setColor(color);
    properties.setName(name.toStdString());

    emit onTrackEdit();
}
