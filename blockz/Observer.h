#ifndef BLOCKZ_OBSERVER_H
#define BLOCKZ_OBSERVER_H


class Observer {
public:
    virtual void update() = 0;

    virtual ~Observer() = default;
};

#endif //BLOCKZ_OBSERVER_H
