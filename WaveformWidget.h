#ifndef BLOCKZ_WAVEFORMWIDGET_H
#define BLOCKZ_WAVEFORMWIDGET_H

#include <QtWidgets/QWidget>

class WaveformWidget : public QWidget {
Q_OBJECT
public:

    explicit WaveformWidget(QString path, QColor color = QColor{255, 102, 0}, QWidget* parent = nullptr);

    explicit WaveformWidget(QString path, std::vector<unsigned int> data, QColor color = QColor{255, 102, 0}, QWidget* parent = nullptr);

    void setSliderPosition(int pos);

    void setColor(const QColor& color);

public slots:
    void setMaximum(unsigned long maximum);

signals:
    void valueChanged(int value);

protected:
    void paintEvent(QPaintEvent* event) override;

    void mouseMoveEvent(QMouseEvent* event) override;

    void mousePressEvent(QMouseEvent* event) override;

private:
    QString path;
    std::vector<unsigned int> data;
    QRect container;
    QVector<QRectF> bars;
    QColor color;
    double barWidth;
    int position;
    int value;
    unsigned long maximum;

    static std::vector<unsigned int> generateDataFromPath(QString path, int size = 400);
    void updateValue(double xCoord);
};


#endif //BLOCKZ_WAVEFORMWIDGET_H
