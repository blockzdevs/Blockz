#include <iostream>
#include <QMenu>
#include <QInputDialog>
#include <QtWidgets/QTableWidgetItem>
#include <QToolBar>
#include <QToolButton>
#include <QButtonGroup>
#include <QtCore/QFileInfo>
#include <QtWidgets/QHBoxLayout>
#include "DashboardView.h"
#include "SetSongDialog.h"
#include "AddTrackDialog.h"
#include "AddBlockDialog.h"
#include "WaveformWidget.h"
#include "EditBlockDialog.h"
#include "EditTrackDialog.h"
#include "BlockLayout.h"

DashboardView::DashboardView(QWidget* parent) :
        QMainWindow{parent},
        controller{std::unique_ptr<DashboardController>{new DashboardController{this}}},
        playButtons{},
        treeModel{new QStandardItemModel{this}},
        headerNames{"Structure view"},
        bpmLabel{new QLabel{this}},
        songNameLabel{new QLabel{this}},
        signatureLabel{new QLabel{this}},
        addTrackAction{nullptr},
        addBlockAction{nullptr},
        playAction{nullptr} {
    ui.setupUi(this);

    setUpToolbar();

    ui.treeView->setAttribute(Qt::WA_MacShowFocusRect, 0);
    ui.treeView->setModel(treeModel);
    ui.treeView->setContextMenuPolicy(Qt::CustomContextMenu);
    QObject::connect(ui.treeView, &QWidget::customContextMenuRequested, this, &DashboardView::onTreeViewContextMenu);

    treeModel->setHorizontalHeaderLabels(headerNames);

    bpmLabel->setText("BPM: <not set>");
    songNameLabel->setText("Song name: <not set>");
    signatureLabel->setText("Signature: <not set>");
    ui.statusbar->addPermanentWidget(bpmLabel);
    ui.statusbar->addPermanentWidget(songNameLabel);
    ui.statusbar->addPermanentWidget(signatureLabel);
}

void DashboardView::setUpToolbar() {
    QToolBar* toolbar = addToolBar("Main toolbar");
    toolbar->setFloatable(false);
    toolbar->setMovable(false);
    toolbar->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);

    QIcon setSongIcon = QIcon::fromTheme("document-new", QIcon{":/res/document-new.svg"});
    QAction* setSongAction = new QAction{setSongIcon, "Set song", this};
    setSongAction->setStatusTip("Set current song properties");
    QObject::connect(setSongAction, &QAction::triggered, this, &DashboardView::onSongSetButtonClicked);
    toolbar->addAction(setSongAction);

    QIcon addTrackIcon = QIcon{":/res/list-add.svg"};
    addTrackAction = new QAction{addTrackIcon, "Add track", this};
    addTrackAction->setStatusTip("Add a new track to current song");
    QObject::connect(addTrackAction, &QAction::triggered, this, &DashboardView::onAddTrackButtonClicked);
    toolbar->addAction(addTrackAction);
    addTrackAction->setEnabled(false);

    QIcon addBlockIcon = QIcon::fromTheme("bookmark-new", QIcon{":/res/bookmark-new.svg"});
    addBlockAction = new QAction{addBlockIcon, "Add block", this};
    addBlockAction->setStatusTip("Add a new block to selected track");
    QObject::connect(addBlockAction, &QAction::triggered, this, &DashboardView::onAddBlockButtonClicked);
    toolbar->addAction(addBlockAction);
    addBlockAction->setEnabled(false);

    QIcon playIcon = QIcon::fromTheme("media-playback-start", QIcon{":/res/media-playback-start.svg"});
    playAction = new QAction{playIcon, "Play song", this};
    playAction->setStatusTip("Start playing the current song (all blocks)");
    QObject::connect(playAction, &QAction::triggered, this, &DashboardView::onPlayButtonClicked);
    toolbar->addAction(playAction);
    playAction->setEnabled(false);

    QIcon stopIcon = QIcon::fromTheme("media-playback-stop", QIcon{":/res/media-playback-stop.svg"});
    stopAction = new QAction{stopIcon, "Stop song", this};
    stopAction->setStatusTip("Stop playing the current song (all blocks)");
    QObject::connect(stopAction, &QAction::triggered, this, &DashboardView::onStopButtonClicked);
    toolbar->addAction(stopAction);
    stopAction->setEnabled(false);

    QWidget* horizontalSpacer = new QWidget(this);
    horizontalSpacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    horizontalSpacer->setVisible(true);
    toolbar->addWidget(horizontalSpacer);
    barLcd = new QLCDNumber;
    barLcd->display(1);
    toolbar->addWidget(barLcd);
    primaryLcd = new QLCDNumber;
    primaryLcd->display(1);
    toolbar->addWidget(primaryLcd);
    secondaryLcd = new QLCDNumber;
    secondaryLcd->display(1);
    toolbar->addWidget(secondaryLcd);

    initTimeCounter();
}

DashboardView::~DashboardView() {}

void DashboardView::update() {
    treeModel->clear();
    treeModel->setHorizontalHeaderLabels(headerNames);

    addBlockAction->setEnabled(!controller->getSong()->getTracks().empty());

    for (const auto& track : controller->getSong()->getTracks()) {
        QStandardItem* trackItem{new QStandardItem{QString::fromStdString(track->getProperties().getName())}};
        trackItem->setData(QVariant::fromValue(track.get()));

        for (const auto& block : track->getBlocks()) {
            QStandardItem* blockItem{new QStandardItem{QString::fromStdString(block->getProperties().getName())}};
            blockItem->setData(QVariant::fromValue(block.get()));
            trackItem->appendRow(blockItem);
        }

        treeModel->appendRow(trackItem);
    }

    ui.treeView->expandAll();

    for (const auto& pair : blockLayouts) {
        Block* block{pair.first};
        BlockLayout* layout{pair.second};

        WaveformWidget* waveformWidget{layout->getWaveform()};
        waveformWidget->setColor(ColorUtils::to_color(pair.first->getProperties().getColor()));
        waveformWidget->update();

        QFileInfo fileInfo{block->getMediaPath()};
        layout->getPathLabel()->setText(fileInfo.fileName());
    }
}

void DashboardView::setTimeUnit(const int& bpm) {
    timeUnit = (250.0/120)*bpm;
}

void DashboardView::setLoopBarsNumber(const int &barsToLoop){
    loopBarsNumber = barsToLoop;
}

void DashboardView::setLoopTime(){
    loopTime = static_cast<double>(secondaryDivisionsPerBar)*timeUnit*static_cast<double>(loopBarsNumber);
}

void DashboardView::initTimeCounter() {
    currentBarLoop = 0;
    primaryDivision = 0;
    secondaryDivision = 0;
    barLcd->display(1);
    primaryLcd->display(1);
    secondaryLcd->display(1);
}

void DashboardView::timeCounter() {
    barsCounter.setTimerType(Qt::PreciseTimer);
    barsCounter.setSingleShot(false);
    barsCounter.start(static_cast<int>(timeUnit));
    QObject::connect(&barsCounter, &QTimer::timeout,
                     this, &DashboardView::secondaryDivisionIncrement, Qt::UniqueConnection);
}

void DashboardView::timeCounterFromResume(int resumeTime) {
    barsCounter.setSingleShot(true);
    resumeTime = resumeTime % static_cast<int>(timeUnit);
    barsCounter.start(resumeTime);
    QObject::connect(&barsCounter, &QTimer::timeout,
                     this, &DashboardView::timeCounter);
    QObject::connect(&barsCounter, &QTimer::timeout,
                     this, &DashboardView::secondaryDivisionIncrement, Qt::UniqueConnection);
}

void DashboardView::loopTrigger() {
    loopTimer.setTimerType(Qt::PreciseTimer);
    loopTimer.setSingleShot(false);
    loopTimer.start(static_cast<int>(loopTime));
    QObject::connect(&loopTimer, &QTimer::timeout,
                     controller.get(), &DashboardController::restart, Qt::UniqueConnection);
    QObject::connect(&loopTimer, &QTimer::timeout,
                     controller.get(), &DashboardController::play, Qt::UniqueConnection);
}

void DashboardView::loopTriggerFromResume(int resumeTime) {
    loopTimer.setSingleShot(true);
    loopTimer.start(resumeTime);
    QObject::connect(&loopTimer, &QTimer::timeout,
                     this, &DashboardView::loopTrigger);
    QObject::connect(&loopTimer, &QTimer::timeout,
                     controller.get(), &DashboardController::play, Qt::UniqueConnection);
}

void DashboardView::currentBarLoopIncrement() {
    currentBarLoop = ((currentBarLoop +1) % loopBarsNumber);
    barLcd->display(currentBarLoop+1);
}

void DashboardView::primaryDivisionIncrement() {
    primaryDivision = (primaryDivision +1) % controller->getSong()->getSignature().numerator();
    if ((primaryDivision % controller->getSong()->getSignature().numerator()) == 0) {
        currentBarLoopIncrement();
    }
    primaryLcd->display(primaryDivision+1);
}

void DashboardView::setSecondaryDivisionsPerBar() {
    secondaryDivisionsPerBar = controller->getSong()->getSignature().numerator()*2;
}


void DashboardView::onSongSetButtonClicked() {
    SetSongDialog* dialog = new SetSongDialog(this);
    dialog->setAttribute(Qt::WA_DeleteOnClose);
    dialog->show();

    QObject::connect(dialog, &SetSongDialog::onClose,
                     this, &DashboardView::onSongSet);
}

void DashboardView::onAddTrackButtonClicked() {
    AddTrackDialog* dialog = new AddTrackDialog(this);
    dialog->setAttribute(Qt::WA_DeleteOnClose);
    dialog->show();

    QObject::connect(dialog, &AddTrackDialog::onTrackAdd, this, &DashboardView::onTrackAdded);
}

void DashboardView::onAddBlockButtonClicked() {
    AddBlockDialog* dialog = new AddBlockDialog(controller->getTracks(), this);
    dialog->setAttribute(Qt::WA_DeleteOnClose);
    dialog->show();

    QObject::connect(dialog, &AddBlockDialog::onBlockAdd, this, &DashboardView::onBlockAdded);
}

void DashboardView::onPlayButtonClicked() {
    stopAction->setEnabled(true);
    if (controller->getSong()->getProperties().getStatus() == PlaybackStatus::Playing) {
        controller->pause();
        resumeTime = loopTimer.remainingTime();
        loopTimer.stop();
        barsCounter.stop();
        playAction->setIcon(QIcon::fromTheme("media-playback-start", QIcon{":/res/media-playback-start.svg"}));
        playAction->setText("Play song");
        playAction->setStatusTip("Start playing the current song (all blocks)");
    } else {
        controller->play();
        if (controller->getSong()->getProperties().getStatus() == PlaybackStatus::Stopped) {
            loopTrigger();
            timeCounter();
        }
        else {
            loopTriggerFromResume(resumeTime);
            timeCounterFromResume(resumeTime);
        }
        playAction->setIcon(QIcon::fromTheme("media-playback-pause", QIcon{":/res/media-playback-pause.svg"}));
        playAction->setText("Pause song");
        playAction->setStatusTip("Pause the current song (all blocks)");
    }
}

void DashboardView::onStopButtonClicked() {
    if (controller->getSong()->getProperties().getStatus() != PlaybackStatus::Stopped) {
        controller->stop();
        loopTimer.stop();
        barsCounter.stop();
        playAction->setIcon(QIcon::fromTheme("media-playback-start", QIcon{":/res/media-playback-start.svg"}));
        playAction->setText("Play song");
        playAction->setStatusTip("Start playing the current song (all blocks)");
        initTimeCounter();
        resumeTime = static_cast<int>(loopTime);
        stopAction->setEnabled(false);
    }
}

void DashboardView::onSongSet(Properties& properties, int& bpm, Signature& signature, int& barsToLoop) {
    Song* oldSong{controller->getSong()};

    if (oldSong) {
        oldSong->removeObserver(this);
    }

    const std::string& title = properties.getName();

    songNameLabel->setText(QString("Song name: %1").arg(QString::fromStdString(title)));
    bpmLabel->setText(QString("BPM: %1").arg(QString::number(bpm)));
    signatureLabel->setText(QString("Signature: %1/%2").arg(signature.numerator()).arg(signature.denominator()));

    addTrackAction->setEnabled(true);

    controller->setSong(std::unique_ptr<Song>{new Song{bpm, signature, properties}});
    controller->getSong()->registerObserver(this);

    setSecondaryDivisionsPerBar();
    setTimeUnit(bpm);
    setLoopBarsNumber(barsToLoop);
    setLoopTime();
}

void DashboardView::onTrackAdded(const QString& name, Color color, float volume) {
    std::unique_ptr<Track> track{new Track};
    Properties properties{PlaybackStatus::Stopped, volume, name.toStdString(), color};
    track->setProperties(properties);
    // TODO handle PlaybackStatus in GUI

    if (!addBlockAction->isEnabled()) {
        addBlockAction->setEnabled(true);
    }

    controller->addTrack(std::move(track));
}

void DashboardView::onBlockAdded(Track* track, QString path, float volume, Color color) {
    auto trackPosition = std::find_if(controller->getTracks().begin(), controller->getTracks().end(),
                                      [&track](const std::unique_ptr<Track>& el) {
                                          return el.get() == track;
                                      });
    int row{0};
    int column{static_cast<int>(track->getBlocks().size())};

    if (trackPosition != controller->getTracks().end()) {
        row = std::distance(controller->getTracks().begin(), trackPosition);
    } else {
        qDebug()  << "[DashboardView::onBlockAdded] Couldn't add block: unknown Track pointer";
        QLabel* errorLabel{new QLabel{"Error while trying to add block."}};
        errorLabel->setStyleSheet("QLabel { color: red; }");
        ui.statusbar->addWidget(errorLabel);

        return;
    }

    Properties properties{PlaybackStatus::Stopped, volume, path.toStdString(), color};
    std::unique_ptr<Block> block{new Block{path, properties}};

    QLabel* loadingLabel{new QLabel{"Loading..."}};
    ui.statusbar->addWidget(loadingLabel);
    QObject::connect(block->getPlayer().get(), &QMediaPlayer::audioAvailableChanged,
                     this, [this, loadingLabel](bool available) {
                if (available) {
                    ui.statusbar->removeWidget(loadingLabel);
                    playAction->setEnabled(true);
                }
            });

    QButtonGroup* buttonGroup;
    auto groupIt = playButtons.find(track);
    if (groupIt == playButtons.end()) {
        buttonGroup = new QButtonGroup;
        playButtons.insert(std::make_pair(track, buttonGroup));
    } else {
        buttonGroup = std::get<1>(*groupIt);
    }

    BlockLayout* blockLayout{new BlockLayout(track, block.get(), path, volume, color, buttonGroup)};
    blockLayouts.insert(std::make_pair(block.get(), blockLayout));

    track->addBlock(std::move(block));
    ui.gridLayout->addWidget(blockLayout, row, column);
}

void DashboardView::onSliderReleased(int position) {
    WaveformWidget* waveform{dynamic_cast<WaveformWidget*>(QObject::sender())};

    if (!waveform) {
        qDebug() << "[DashboardView::onSliderReleased()] WARNING: QObject::sender() returned nullptr";
        return;
    }

    Block* block{waveform->property("block").value<Block*>()};
    block->setPosition(position);
}

void DashboardView::onTreeViewContextMenu(const QPoint& pos) {
    QModelIndex index{ui.treeView->indexAt(pos)};
    QMenu* contextMenu{new QMenu};

    if (index.isValid()) {
        if (index.parent() == QModelIndex{}) { // index points to top-level item (i.e. a Track)
            QAction* editTrackAction{new QAction{"Edit track"}};
            QAction* removeTrackAction{new QAction{"Remove track"}};
            QAction* muteTrackAction{new QAction{"Mute track"}};
            muteTrackAction->setCheckable(true);

            Track* track{treeModel->itemFromIndex(index)->data().value<Track*>()};
            muteTrackAction->setChecked(track->isMute());

            QObject::connect(editTrackAction, &QAction::triggered, this, [this, track](){
                EditTrackDialog* dialog{new EditTrackDialog{track}};
                dialog->setAttribute(Qt::WA_DeleteOnClose);

                QObject::connect(dialog, &EditTrackDialog::onTrackEdit, this, [this]() {
                    update();
                });

                dialog->show();
            });
            QObject::connect(removeTrackAction, &QAction::triggered, this, [this, track](){
                for (const auto& block : track->getBlocks()) {
                    delete blockLayouts[block.get()];
                    blockLayouts.erase(block.get());
                }

                controller->removeTrack(track);

                update();
            });
            QObject::connect(muteTrackAction, &QAction::toggled, this, [this, track](bool checked) {
                track->setMute(checked);
            });

            contextMenu->addAction(editTrackAction);
            contextMenu->addAction(removeTrackAction);
            contextMenu->addAction(muteTrackAction);
        } else { // user right-clicked on a block
            QAction* editBlockAction{new QAction{"Edit block"}};
            QAction* removeBlockAction{new QAction{"Remove block"}};

            Block* block{treeModel->itemFromIndex(index)->data().value<Block*>()};
            Track* track{treeModel->itemFromIndex(index.parent())->data().value<Track*>()};

            QObject::connect(editBlockAction, &QAction::triggered, this, [this, track, block](){
                EditBlockDialog* dialog{new EditBlockDialog{track, block}};
                dialog->setAttribute(Qt::WA_DeleteOnClose);

                QObject::connect(dialog, &EditBlockDialog::onBlockEdit, this, [this]() {
                    update();
                });

                dialog->show();
            });

            QObject::connect(removeBlockAction, &QAction::triggered, this, [this, track, block](){
                ui.gridLayout->removeWidget(blockLayouts[block]);

                delete blockLayouts[block];
                blockLayouts.erase(block);

                track->removeBlock(block);

                update();
            });

            contextMenu->addAction(editBlockAction);
            contextMenu->addAction(removeBlockAction);
        }

        contextMenu->exec(ui.treeView->mapToGlobal(pos));
    }
}

void DashboardView::secondaryDivisionIncrement() {
    secondaryDivision = (secondaryDivision + 1) % secondaryDivisionsPerBar;
    if ((secondaryDivision % 2) == 0) {
        primaryDivisionIncrement();
    }
    secondaryLcd->display(-secondaryLcd->intValue()); //QT BUG: it looks like QLCDNumber::display(int n) doesn't display
    secondaryLcd->display(secondaryDivision+1);       //the argument straight away but sums instead that argument to the
    //value previously displayed, if one was displayed before
}
