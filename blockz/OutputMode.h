#ifndef BLOCKZ_OUTPUTMODE_H
#define BLOCKZ_OUTPUTMODE_H

enum class OutputMode {
    Stereo,
    Left,
    Right
};

Q_DECLARE_METATYPE(OutputMode)

#endif