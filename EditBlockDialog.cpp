#include "EditBlockDialog.h"

EditBlockDialog::EditBlockDialog(Track* track, Block* block, QWidget* parent) :
        AddBlockDialog(std::list<std::unique_ptr<Track>>{}, parent),
        block{block},
        track{track} {
    path = block->getMediaPath();

    ui.trackInput->addItem(QString::fromStdString(track->getProperties().getName()), QVariant::fromValue(track));
    ui.trackInput->setCurrentIndex(0);

    ui.fileLabel->setText(block->getMediaPath());
    ui.trackInput->setCurrentText(QString::fromStdString(block->getProperties().getName()));

    int colorIndex = ui.colorInput->findData(QVariant::fromValue(block->getProperties().getColor()));
    if (colorIndex == -1) {
        qDebug() << Q_FUNC_INFO << "Error: invalid color";
        return;
    }

    ui.colorInput->setCurrentIndex(colorIndex);
}

void EditBlockDialog::on_buttonBox_accepted() {
    float volume{static_cast<float>(ui.volumeSlider->value())};
    Color color{ui.colorInput->currentData().value<Color>()};
    QString mediaPath{ui.fileLabel->text()};

    Properties& properties{block->getProperties()};
    properties.setVolume(volume);
    properties.setColor(color);
    properties.setName(mediaPath.toStdString());

    block->setMediaPath(mediaPath);

    if (!path.isEmpty()) {
        emit onBlockEdit();
    }
}
