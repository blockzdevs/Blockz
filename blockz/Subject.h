#ifndef BLOCKZ_SUBJECT_H
#define BLOCKZ_SUBJECT_H

#include <list>
#include "Observer.h"

class Subject {
public:

    void notify();

    void registerObserver(Observer* observer);

    void removeObserver(Observer* observer);

    virtual ~Subject() = default;

private:
    std::list<Observer*> observers;
};


#endif //BLOCKZ_SUBJECT_H
