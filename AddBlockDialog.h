#ifndef BLOCKZ_ADDBLOCKDIALOG_H
#define BLOCKZ_ADDBLOCKDIALOG_H

#include <QtWidgets/QDialog>
#include <blockz/Track.h>
#include "blockz/Color.h"
#include "ui_addblockdialog.h"

class AddBlockDialog : public QDialog {
Q_OBJECT

public:
    explicit AddBlockDialog(const std::list<std::unique_ptr<Track>>& tracks, QWidget* parent);

public slots:

    virtual void on_fileChooserButton_clicked();
    virtual void on_buttonBox_accepted();

signals:
    void onBlockAdd(Track* track, QString blockPath, float volume, Color color);

protected:
    Ui::AddBlockDialog ui;
    QString path;

private:
    const std::list<std::unique_ptr<Track>>& tracks;
    const int initialWidth;
};


#endif //BLOCKZ_ADDBLOCKDIALOG_H
