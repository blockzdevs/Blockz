cmake_minimum_required(VERSION 3.6)
project(blockz_test)

set(CMAKE_CXX_STANDARD 11)

if (CMAKE_COMPILER_IS_GNUCC)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -pedantic")
endif (CMAKE_COMPILER_IS_GNUCC)

if (MSVC)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /W4")
endif (MSVC)

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTOUIC ON)

find_package(Qt5Core)
find_package(Qt5Test)

set(SOURCE_FILES SongTest.cpp runTests.cpp ObserverTest.cpp SongTest.h ObserverTest.h)
add_executable(blockz_test ${SOURCE_FILES})
target_link_libraries(blockz_test blockz_core Qt5::Core Qt5::Test)
