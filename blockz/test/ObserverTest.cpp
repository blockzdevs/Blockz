#include "ObserverTest.h"

void ObserverTest::observer_notify() {
    observer.setSubject(&subject);

    //EXPECT_CALL(observer, update()).Times(1); // TODO: find equivalent QtTest construct

    subject.setData(1);
}

void ObserverTest::observer_add_remove() {
    observer.setSubject(&subject);

    //EXPECT_CALL(observer, update()).Times(0); // TODO: find equivalent QtTest construct

    subject.removeObserver(&observer);

    subject.setData(1);
}