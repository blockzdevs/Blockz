#include <QApplication>
#include "blockz/Song.h"
#include "DashboardView.h"

int main(int argc, char** argv) {
    
    QApplication app{argc, argv};
    app.setWindowIcon(QIcon(":/res/Blockz1.png"));
    app.setStyleSheet("QTreeView { background-color: rgb(25, 25, 25, 200);"
                              "border-radius: 10px;}"
                              "SetSongDialog {background-color: rgb(25, 25, 25);}"
                              "AddTrackDialog {background-color: rgb(25, 25, 25);}"
                              "AddBlockDialog {background-color: rgb(25, 25, 25);}"
                              "QObject {color: rgb(125, 125, 125);}"  //this sets texts colour
                              "QLineEdit {background-color: rgb(60, 60, 60);"
                              "border-color: rgb(125, 125, 125);}" //FIXME: border-color not working
                              "QPushButton { color: black }");
    DashboardView dv;
    dv.setStyleSheet("QLabel { color: rgb(125, 125, 125);}");  //this sets status bar text colour
    dv.setStyleSheet("QMainWindow {border-image: url(:/res/mainwindow-background.png);}"
                             "QStatusBar {background-color: rgb(35, 35,35, 200);}"
                             "QToolBar {background: rgb(35, 35, 35); border: none;}");
    dv.showMaximized();

    return app.exec();
}