#include <iostream>
#include <QtGui/QStandardItemModel>
#include "SetSongDialog.h"
#include <QPushButton>

SetSongDialog::SetSongDialog(QWidget* parent) :
        QDialog{parent},
        validName{false},
        validBars{true} {
    ui.setupUi(this);
    ui.barsToLoopText->setAttribute(Qt::WA_MacShowFocusRect, 0);
    ui.nameText->setAttribute(Qt::WA_MacShowFocusRect, 0);
    ui.errorLabel->setVisible(true);

    ui.buttonBox->button(QDialogButtonBox::StandardButton::Ok)->setEnabled(false);
    ui.signatureInput->addItem("4/4", QVariant::fromValue(Signature(4, 4)));
    ui.signatureInput->addItem("2/4", QVariant::fromValue(Signature(2, 4)));
    ui.signatureInput->addItem("3/4", QVariant::fromValue(Signature(3, 4)));
    ui.signatureInput->addItem("6/8", QVariant::fromValue(Signature(6, 8)));
    ui.signatureInput->addItem("7/8", QVariant::fromValue(Signature(7, 8)));
    ui.signatureInput->addItem("15/16", QVariant::fromValue(Signature(15, 16)));

    QObject::connect(ui.nameText, &QLineEdit::textChanged, this, &SetSongDialog::onValidNameInput);
    QObject::connect(ui.barsToLoopText, &QLineEdit::textChanged, this, &SetSongDialog::onValidBarsInput);
}

void SetSongDialog::on_buttonBox_accepted() {
    this->name = ui.nameText->text();
    Properties properties{PlaybackStatus::Stopped, 0.0f, name.toStdString(), Color::Blue};
    int bpm = ui.bpmInput->text().toInt(); // TODO: handle possible errors
    Signature sig = ui.signatureInput->currentData().value<Signature>();
    int barsToLoop = ui.barsToLoopText->text().toInt(); //TODO: handle possible errors

    emit onClose(properties, bpm, sig, barsToLoop);
}

void SetSongDialog::onValidNameInput(const QString& text) {
    validName = !text.isEmpty();

    setControlsEnabled(validName && validBars);
}

void SetSongDialog::onValidBarsInput(const QString& text) {
    bool validBars = !text.isEmpty() && QRegExp("\\d*").exactMatch(text);

    setControlsEnabled(validName && validBars);
}

void SetSongDialog::setControlsEnabled(bool enabled) {
    QPushButton* okButton{ui.buttonBox->button(QDialogButtonBox::StandardButton::Ok)};

    okButton->setEnabled(enabled);
    ui.errorLabel->setVisible(!enabled);
}
