
#include "Color.h"

std::string ColorUtils::to_string(Color color) {
    switch (color) {
        case Color::Red:
            return "Red";
        case Color::Yellow:
            return "Yellow";
        case Color::Blue:
            return "Blue";
        case Color::Green:
            return "Green";
    }

    return "<invalid color>";
}

QColor ColorUtils::to_color(Color color) {
    switch (color) {
        case Color::Red:
            return QColor{0xFF, 0x41, 0x36};
        case Color::Yellow:
            return QColor{0xFF, 0xDC, 0x00};
        case Color::Blue:
            return QColor{0x00, 0x74, 0xD9};
        case Color::Green:
            return QColor{0x2E, 0xCC, 0x40};
    }

    return QColor{};
}
