#ifndef BLOCKZ_BLOCKLAYOUT_H
#define BLOCKZ_BLOCKLAYOUT_H

#include "WaveformWidget.h"

class BlockLayout : public QWidget {
Q_OBJECT
public:
    BlockLayout(Track* track, Block* block, QString path, float volume, Color color, QButtonGroup* buttonGroup);

    QVBoxLayout* getLayout() const;

    QLabel* getPathLabel() const;

    WaveformWidget* getWaveform() const;

public slots:
    void onSliderReleased(int position);

private:
    QVBoxLayout* layout;
    QLabel* pathLabel;
    WaveformWidget* waveform;
};


#endif //BLOCKZ_BLOCKLAYOUT_H
