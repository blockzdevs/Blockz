#ifndef BLOCKZ_SIGNATURE_H
#define BLOCKZ_SIGNATURE_H

#include <QtCore/QMetaType>

class Signature {
public:
    explicit Signature(int numerator = 4, int denominator = 4);

    int numerator() const;
    int denominator() const;

private:
    int _numerator;
    int _denominator;
};

Q_DECLARE_METATYPE(Signature)

#endif //BLOCKZ_SIGNATURE_H
