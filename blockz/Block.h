#ifndef BLOCKZ_BLOCK_H
#define BLOCKZ_BLOCK_H

#include <memory>
#include <QString>
#include <QtMultimedia/QMediaPlayer>
#include "Properties.h"

class Block : public QObject {
Q_OBJECT
public:
    explicit Block(QString mediaPath, Properties properties = Properties{PlaybackStatus::Stopped, 0.0f, "Unnamed block", Color::Red});

    bool operator==(const Block& rhs) const;

    bool operator!=(const Block& rhs) const;

    const std::unique_ptr<QMediaPlayer>& getPlayer() const;

    void setPosition(const int position);

    const Properties& getProperties() const;

    Properties& getProperties();

    const QString& getMediaPath() const;

    void setMediaPath(const QString& mediaPath);

    void setMute(bool muted);

    bool isMute();

public slots:
    void onError(QMediaPlayer::Error error);

private:
    Properties properties;
    std::unique_ptr<QMediaPlayer> player;
    QString mediaPath;

};

#endif
