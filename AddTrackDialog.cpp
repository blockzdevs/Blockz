#include <blockz/Color.h>
#include <QtWidgets/QFileDialog>
#include <QPushButton>
#include <blockz/OutputMode.h>
#include "AddTrackDialog.h"

AddTrackDialog::AddTrackDialog(QWidget* parent) : QDialog{parent} {
    ui.setupUi(this);
    ui.colorInput->setAttribute(Qt::WA_MacShowFocusRect, 0);
    ui.volumeSlider->setAttribute(Qt::WA_MacShowFocusRect, 0);
    ui.nameInput->setAttribute(Qt::WA_MacShowFocusRect, 0);
    ui.outputModeInput->setAttribute(Qt::WA_MacShowFocusRect, 0);
    ui.buttonBox->button(QDialogButtonBox::StandardButton::Ok)->setEnabled(false);

    ui.colorInput->insertItem(1, "Red", QVariant::fromValue(Color::Red));
    ui.colorInput->insertItem(2, "Green", QVariant::fromValue(Color::Green));
    ui.colorInput->insertItem(3, "Blue", QVariant::fromValue(Color::Blue));

    ui.outputModeInput->insertItem(1, "Stereo", QVariant::fromValue(OutputMode::Stereo));
    ui.outputModeInput->insertItem(2, "Left", QVariant::fromValue(OutputMode::Left));
    ui.outputModeInput->insertItem(3, "Right",QVariant::fromValue(OutputMode::Right));

    QObject::connect(ui.nameInput, &QLineEdit::textChanged, this, &AddTrackDialog::onValidNameInput);
}

void AddTrackDialog::on_buttonBox_accepted() {
    float volume{static_cast<float>(ui.volumeSlider->value())};
    Color color{ui.colorInput->currentData().value<Color>()};

    emit onTrackAdd(ui.nameInput->text(), color, volume);
}

void AddTrackDialog::onValidNameInput(const QString& text) {
    QPushButton* okButton{ui.buttonBox->button(QDialogButtonBox::StandardButton::Ok)};
    bool validName{!text.isEmpty()};

    okButton->setEnabled(validName);
    ui.errorLabel->setVisible(!validName);
}

void AddTrackDialog::setControlsEnabled(bool enabled) {
    ui.buttonBox->button(QDialogButtonBox::StandardButton::Ok)->setEnabled(enabled);
    ui.errorLabel->setVisible(!enabled);
}
