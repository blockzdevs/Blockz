#include "Signature.h"

Signature::Signature(int numerator, int denominator) :
        _numerator{numerator}, _denominator{denominator} {}

int Signature::numerator() const {
    return _numerator;
}

int Signature::denominator() const {
    return _denominator;
}
