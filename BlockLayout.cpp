#include <QtGui/QIcon>
#include <blockz/Block.h>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QLayout>
#include <QtWidgets/QLabel>
#include <QtCore/QFileInfo>
#include <blockz/Track.h>
#include "BlockLayout.h"
#include "WaveformWidget.h"

BlockLayout::BlockLayout(Track* track, Block* block, QString path, float volume, Color color, QButtonGroup* buttonGroup) :
        layout{new QVBoxLayout{this}}{
    QHBoxLayout* firstRow{new QHBoxLayout};

    QLabel* pathLabel{new QLabel};
    QFileInfo fileInfo{path};
    pathLabel->setWordWrap(true); // TODO: Use auto-ellipsized QLabel custom class instead of word-wrapping
    pathLabel->setText(fileInfo.fileName());
    this->pathLabel = pathLabel;

    WaveformWidget* waveform{new WaveformWidget{path, ColorUtils::to_color(color)}};
    this->waveform = waveform;

    QObject::connect(block->getPlayer().get(), &QMediaPlayer::durationChanged,
                     waveform, &WaveformWidget::setMaximum);

    QObject::connect(block->getPlayer().get(), &QMediaPlayer::positionChanged,
                     waveform, [waveform](int pos) {
                waveform->setSliderPosition(pos);
            });
    QObject::connect(waveform, &WaveformWidget::valueChanged, this, &BlockLayout::onSliderReleased);

    waveform->setProperty("block", QVariant::fromValue(block));

    QHBoxLayout* buttonLayout{new QHBoxLayout};

    buttonLayout->setSizeConstraint(QLayout::SizeConstraint::SetFixedSize);
    buttonLayout->setAlignment(Qt::AlignRight | Qt::AlignTop);

    QToolButton* playButton{new QToolButton};
    playButton->setIcon(QIcon::fromTheme("media-playback-pause", QIcon{":/res/media-playback-pause.svg"}));

    buttonGroup->addButton(playButton);

    buttonLayout->addWidget(playButton);

    // FIXME: State changes should propagate to UI, not vice versa (as here)
    QObject::connect(playButton, &QAbstractButton::clicked, this, [waveform, track, buttonGroup, playButton](bool checked) {
        if (!checked) {
            for (const auto& btn : buttonGroup->buttons()) {
                btn->setIcon(QIcon::fromTheme("media-playback-pause", QIcon{":/res/media-playback-pause.svg"}));
                btn->setCheckable(false);
                btn->setChecked(false);
            }

            playButton->setIcon(QIcon::fromTheme("media-playback-start", QIcon{":/res/media-playback-start.svg"}));
            playButton->setCheckable(true);
            playButton->setChecked(true);

            Block* block{waveform->property("block").value<Block*>()};
            track->setPlayingBlock(block);
        } else {
            playButton->setCheckable(false);
            playButton->setChecked(false);
            playButton->setIcon(QIcon::fromTheme("media-playback-pause", QIcon{":/res/media-playback-pause.svg"}));

            track->setPlayingBlock(nullptr);
        }
    });

    firstRow->addWidget(pathLabel);
    firstRow->addLayout(buttonLayout);
    layout->addLayout(firstRow);
    layout->addWidget(waveform);
}

void BlockLayout::onSliderReleased(int position) {
    WaveformWidget* waveform{dynamic_cast<WaveformWidget*>(QObject::sender())};

    if (!waveform) {
        qDebug() << "[DashboardView::onSliderReleased()] WARNING: QObject::sender() returned nullptr";
        return;
    }

    Block* block{waveform->property("block").value<Block*>()};
    block->setPosition(position);
}

QLabel* BlockLayout::getPathLabel() const {
    return pathLabel;
}

WaveformWidget* BlockLayout::getWaveform() const {
    return waveform;
}

QVBoxLayout* BlockLayout::getLayout() const {
    return layout;
}
