#ifndef BLOCKZ_MUSICPROJECT_H
#define BLOCKZ_MUSICPROJECT_H

#include <list>
#include <string>
#include <memory>
#include "Properties.h"
#include "Track.h"
#include "Signature.h"

class Song : public Subject {
public:
    explicit Song(int bpm = 120, Signature signature = Signature(4, 4), Properties properties = Properties{PlaybackStatus::Stopped, 0.0f, "Unnamed song", Color::Blue});

    int getBpm() const;

    void setBpm(int bpm);

    const Signature& getSignature() const;

    void setSignature(const Signature& signature);

    const std::list<std::unique_ptr<Track>>& getTracks() const;

    void addTrack(std::unique_ptr<Track> track);

    void removeTrack(const Track* track);

    Properties& getProperties() {
        return properties;
    }

    const Properties& getProperties() const {
        return properties;
    }

private:
    int bpm;
    Signature signature;
    std::list<std::unique_ptr<Track>> tracks;
    Properties properties;
};


#endif //BLOCKZ_MUSICPROJECT_H
