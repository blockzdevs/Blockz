#ifndef BLOCKZ_EDITTRACKDIALOG_H
#define BLOCKZ_EDITTRACKDIALOG_H

#include <blockz/Track.h>
#include "AddTrackDialog.h"

class EditTrackDialog : public AddTrackDialog {
Q_OBJECT
public:
    EditTrackDialog(Track* track, QWidget* parent = nullptr);

    void on_buttonBox_accepted() override;

signals:
    void onTrackEdit();

private:
    Track* track;
};


#endif //BLOCKZ_EDITTRACKDIALOG_H
