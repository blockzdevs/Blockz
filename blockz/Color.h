#ifndef BLOCKZ_COLOR_H
#define BLOCKZ_COLOR_H

#include <QtCore/QMetaType>
#include <QtGui/QColor>

enum class Color {
    Red,
    Yellow,
    Blue,
    Green
};

namespace ColorUtils {
    std::string to_string(Color color);

    QColor to_color(Color color);
}

Q_DECLARE_METATYPE(Color)

#endif