#include "DashboardController.h"
#include "DashboardView.h"

DashboardController::DashboardController(DashboardView* view) : view{view},
                                                                song{nullptr} {}

void DashboardController::setSong(std::unique_ptr<Song> song) {
    DashboardController::song = std::move(song);
}

Song* DashboardController::getSong() {
    return song.get();
}

void DashboardController::addTrack(std::unique_ptr<Track> track) {
    song->addTrack(std::move(track));
    song->getTracks().back()->registerObserver(view);
}

const std::list<std::unique_ptr<Track>>& DashboardController::getTracks() {
    return song->getTracks();
}

void DashboardController::play() {
    for (const std::unique_ptr<Track>& track : song->getTracks()) {
        track->play();
    }
    song->getProperties().setStatus(PlaybackStatus::Playing);
}

void DashboardController::pause() {
    for (const std::unique_ptr<Track>& track : song->getTracks()) {
        track->pause();
    }
    song->getProperties().setStatus(PlaybackStatus::Paused);
}

void DashboardController::stop() {
    for (const std::unique_ptr<Track>& track : song->getTracks()) {
        track->stop();
    }
    song->getProperties().setStatus(PlaybackStatus::Stopped);
}

void DashboardController::restart() {
    for (const std::unique_ptr<Track>& track : song->getTracks()) {
        track->restart();
    }
}

void DashboardController::setMuteTrack(std::unique_ptr<Track> track) {
    if(track->getProperties().getStatus() == PlaybackStatus::Playing){
        track->getProperties().setStatus(PlaybackStatus::Stopped);
    } else {
        track->getProperties().setStatus(PlaybackStatus::Playing);
    }
}

void DashboardController::removeTrack(Track* track) {
    song->removeTrack(track);
}
