#ifndef BLOCKZ_ADDTRACKDIALOG_H
#define BLOCKZ_ADDTRACKDIALOG_H

#include <QtWidgets/QDialog>
#include "blockz/Color.h"
#include "ui_addtrackdialog.h"

class AddTrackDialog : public QDialog {
Q_OBJECT

public:
    explicit AddTrackDialog(QWidget* parent = nullptr);

public slots:

    virtual void on_buttonBox_accepted();
    void onValidNameInput(const QString& text);

signals:
    void onTrackAdd(const QString& name, Color color, float volume);

protected:
    Ui::AddTrackDialog ui;

private:
    QString path;

    void setControlsEnabled(bool enabled);
};


#endif //BLOCKZ_ADDTRACKDIALOG_H
