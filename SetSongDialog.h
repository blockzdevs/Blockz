#ifndef BLOCKZ_ADDDIALOG_H
#define BLOCKZ_ADDDIALOG_H

#include <QtWidgets/QDialog>
#include <blockz/Properties.h>
#include <blockz/Signature.h>
#include "ui_setsongdialog.h"

class SetSongDialog : public QDialog {
Q_OBJECT

public:
    explicit SetSongDialog(QWidget* parent = nullptr);

public slots:
    void on_buttonBox_accepted();
    void onValidNameInput(const QString& text);
    void onValidBarsInput(const QString& text);

signals:
    void onClose(Properties& properties, int& bpm, Signature& signature, int& barsNumberOnReproduce);

private:
    Ui::SetSongDialog ui;
    QString name;
    bool validName;
    bool validBars;

    void setControlsEnabled(bool enabled);
};


#endif //BLOCKZ_ADDDIALOG_H
