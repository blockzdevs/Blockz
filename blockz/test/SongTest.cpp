#include <QTest>
#include "../Block.h"
#include "../Song.h"
#include "SongTest.h"

void SongTest::song_track_add() {
    Track track;

    song.addTrack(track);

    QVERIFY(song.getTracks().size() == 1);
    QVERIFY(song.getTracks().front() == track);
}

void SongTest::song_track_remove() {
    Track track;

    song.addTrack(track);
    song.removeTrack(track);

    QVERIFY(song.getTracks().size() == 0);
}

void SongTest::song_props_get_set() {
    Song song{120, Signature(4, 4), Properties{PlaybackStatus::Playing, 10.0f, "Foo", Color::Red}};

    Properties properties{PlaybackStatus::Playing, 10.0f, "Foo", Color::Red};

    QVERIFY(song.getProperties() == properties);
}