#ifndef BLOCKZ_PROPERTIES_H
#define BLOCKZ_PROPERTIES_H

#include <string>
#include "PlaybackStatus.h"
#include "Color.h"
#include "Subject.h"

class Properties : public Subject {
public:

    Properties(PlaybackStatus status, float volume, const std::string& name, Color color) : status(status),
                                                                                            volume(volume),
                                                                                            name(name),
                                                                                            color(color) {}
    bool operator==(const Properties& rhs) const {
        return status == rhs.status &&
               volume == rhs.volume &&
               name == rhs.name &&
               color == rhs.color;
    }

    bool operator!=(const Properties& rhs) const {
        return !(rhs == *this);
    }

    PlaybackStatus getStatus() const {
        return status;
    }

    void setStatus(PlaybackStatus status) {
        Properties::status = status;
        notify();
    }

    float getVolume() const {
        return volume;
    }

    void setVolume(float volume) {
        Properties::volume = volume;
        notify();
    }

    const std::string& getName() const {
        return name;
    }

    void setName(const std::string& name) {
        Properties::name = name;
        notify();
    }

    Color getColor() const {
        return color;
    }

    void setColor(Color color) {
        Properties::color = color;
        notify();
    }

private:
    PlaybackStatus status;
    float volume;
    std::string name;
    Color color;
};

#endif //BLOCKZ_PROPERTIES_H
