#ifndef BLOCKZ_TRACK_H
#define BLOCKZ_TRACK_H

#include <string>
#include <list>
#include <memory>
#include "Properties.h"
#include "Block.h"
#include "OutputMode.h"

class Track : public QObject, public Subject {
Q_OBJECT
public:

    Track();

    bool operator==(const Track& rhs) const;

    bool operator!=(const Track& rhs) const;

    void play();
    void pause() const;
    void stop();

    void addBlock(std::unique_ptr<Block> block);
    void removeBlock(Block* block);
    const std::list<std::unique_ptr<Block>>& getBlocks() const;
    void setPlayingBlock(Block* block);

    void setOutputMode(OutputMode outputMode);
    const Properties& getProperties() const;

    Properties& getProperties();

    void setProperties(const Properties& properties);

    void restart();

    void setMute(bool muted);

    bool isMute();

private:
    std::list<std::unique_ptr<Block>> blocks;
    std::list<std::unique_ptr<Block>>::const_iterator playingBlock;
    Properties properties;
    OutputMode outputMode;
};

#endif //BLOCKZ_TRACK_H
