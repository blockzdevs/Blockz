#include <blockz/Color.h>
#include <QtWidgets/QFileDialog>
#include <QtCore/QStandardPaths>
#include "AddBlockDialog.h"

AddBlockDialog::AddBlockDialog(const std::list<std::unique_ptr<Track>>& tracks, QWidget* parent) :
        QDialog{parent},
        tracks{tracks},
        initialWidth{width()} {
    ui.setupUi(this);
    ui.colorInput->setAttribute(Qt::WA_MacShowFocusRect, 0);
    ui.trackInput->setAttribute(Qt::WA_MacShowFocusRect, 0);
    ui.volumeSlider->setAttribute(Qt::WA_MacShowFocusRect, 0);

    ui.colorInput->insertItem(1, "Red", QVariant::fromValue(Color::Red));
    ui.colorInput->insertItem(2, "Green", QVariant::fromValue(Color::Green));
    ui.colorInput->insertItem(3, "Blue", QVariant::fromValue(Color::Blue));
    ui.colorInput->insertItem(4, "Yellow", QVariant::fromValue(Color::Yellow));

    for (const auto& track : tracks) {
        ui.trackInput->addItem(QString::fromStdString(track->getProperties().getName()), QVariant::fromValue(track.get()));
    }
}

void AddBlockDialog::on_fileChooserButton_clicked() {
    QString path = QFileDialog::getOpenFileName(this,
                                                "Choose audio file",
                                                QStandardPaths::writableLocation(QStandardPaths::MusicLocation),
                                                "Sound files: (*.mp3 *.wav *.ogg)");
    ui.fileLabel->setText(QString("File: %1").arg(path));

    QFontMetrics metrics(ui.fileLabel->font());
    int width = initialWidth * 3;
    QString clippedText = metrics.elidedText(path, Qt::ElideLeft, width);
    ui.fileLabel->setText(clippedText);
    ui.fileLabel->setToolTip(path);

    this->path = path;
}

void AddBlockDialog::on_buttonBox_accepted() {
    float volume = ui.volumeSlider->value();
    Color color = ui.colorInput->currentData().value<Color>();
    Track* track = ui.trackInput->currentData().value<Track*>();

    if (!path.isEmpty()) {
        emit onBlockAdd(track, path, volume, color);
    }
}
