#include "Track.h"

using std::string;

Track::Track() :
        blocks{},
        playingBlock{blocks.end()},
        properties{PlaybackStatus::Stopped, 0.0f, "Unnamed Track", Color::Green},
        outputMode{OutputMode::Stereo} {}

void Track::addBlock(std::unique_ptr<Block> block) {
    blocks.push_back(std::move(block));
    notify();
}

void Track::removeBlock(Block* block) {
    blocks.remove_if([block](const std::unique_ptr<Block>& el) {
        return el.get() == block;
    });

    notify();
}

void Track::setOutputMode(OutputMode outputMode) {
    Track::outputMode = outputMode;
}

bool Track::operator==(const Track& rhs) const {
    return blocks == rhs.blocks &&
           properties == rhs.properties &&
           outputMode == rhs.outputMode;
}

bool Track::operator!=(const Track& rhs) const {
    return !(rhs == *this);
}

void Track::play() {
    playingBlock = std::find_if(blocks.cbegin(), blocks.cend(), [](const std::unique_ptr<Block>& block) {
        return block->getProperties().getStatus() == PlaybackStatus::Playing;
    });

    if (playingBlock != blocks.end()) {
        playingBlock->get()->getPlayer()->play();
    }
}

void Track::pause() const {
    if (playingBlock != blocks.end()) {
        playingBlock->get()->getPlayer()->pause();
    }
}

void Track::stop() {
    playingBlock = std::find_if(blocks.cbegin(), blocks.cend(), [](const std::unique_ptr<Block>& block) {
        return block->getProperties().getStatus() != PlaybackStatus::Stopped;
    });

    if(playingBlock != blocks.end()) {
        playingBlock->get()->getPlayer()->stop();
    }
}

const std::list<std::unique_ptr<Block>>& Track::getBlocks() const {
    return blocks;
}

const Properties& Track::getProperties() const {
    return properties;
}

Properties& Track::getProperties() {
    return properties;
}

void Track::setProperties(const Properties& properties) {
    Track::properties = properties;
}

void Track::setPlayingBlock(Block* block) {
    std::for_each(blocks.begin(), blocks.end(), [block](std::unique_ptr<Block>& el) {
        PlaybackStatus status{PlaybackStatus::Stopped};

        if (el.get() == block) {
            status = PlaybackStatus::Playing;
        }

        el->getProperties().setStatus(status);
    });
}

void Track::restart() {
    if(playingBlock != blocks.end()) {
        playingBlock->get()->setPosition(0);
    }
}
void Track::setMute(bool muted) {
    for (const auto& block : blocks) {
        block->setMute(muted);
    }
}

bool Track::isMute() {
    bool mute{true};

    for (const auto& block : blocks) {
        mute &= block->isMute();
    }

    return mute;
}
