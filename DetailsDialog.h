#ifndef BLOCKZ_TRACKSDIALOG_H
#define BLOCKZ_TRACKSDIALOG_H

#include <QtWidgets/QDialog>
#include "ui_detailsdialog.h"

class DetailsDialog : public QDialog {
    Q_OBJECT

public:
    explicit DetailsDialog(QWidget* parent = nullptr);

private:
    Ui::DetailsDialog ui;
};


#endif //BLOCKZ_TRACKSDIALOG_H
