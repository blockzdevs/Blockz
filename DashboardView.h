#ifndef BLOCKZ_DASHBOARDVIEW_H
#define BLOCKZ_DASHBOARDVIEW_H

#include <memory>
#include <QtWidgets/QMainWindow>
#include <blockz/Song.h>
#include <QtGui/QStandardItemModel>
#include <QtWidgets/QLabel>
#include "DashboardController.h"
#include "ui_dashboardview.h"
#include "WaveformWidget.h"
#include "BlockLayout.h"
#include <QTimer>
#include <unordered_map>
#include <QLCDNumber>

class DashboardView : public QMainWindow, public Observer {
    Q_OBJECT

public:
    explicit DashboardView(QWidget* parent = nullptr);
    ~DashboardView() override;

    void update() override;
    void setTimeUnit(const int& bpm);
    void setLoopBarsNumber(const int &barsToLoop);
    void setLoopTime();
    void initTimeCounter();
    void timeCounter();
    void timeCounterFromResume(int timeUnit);
    void loopTrigger();
    void loopTriggerFromResume(int timeUnit);
    void currentBarLoopIncrement();
    void primaryDivisionIncrement();
    void setSecondaryDivisionsPerBar();

public slots:
    void onSongSetButtonClicked();
    void onAddTrackButtonClicked();
    void onAddBlockButtonClicked();
    void onPlayButtonClicked();
    void onStopButtonClicked();
    void onSongSet(Properties& properties, int& bpm, Signature& signature,  int& barsToLoop);
    void onTrackAdded(const QString& name, Color color, float volume);
    void onBlockAdded(Track* track, QString path, float volume, Color color);
    void onSliderReleased(int position);
    void onTreeViewContextMenu(const QPoint& pos);
    void secondaryDivisionIncrement();

private:
    double timeUnit;
    double loopTime;
    int loopBarsNumber;
    int currentBarLoop;
    int primaryDivision;
    int secondaryDivision;
    int resumeTime = 0;
    int secondaryDivisionsPerBar;

    Ui::DashboardView ui;
    std::unique_ptr<DashboardController> controller;
    std::unordered_map<Track*, QButtonGroup*> playButtons;
    std::unordered_map<Block*, BlockLayout*> blockLayouts;
    QStandardItemModel* treeModel;

    const QStringList headerNames;

    QLabel* bpmLabel;
    QLabel* songNameLabel;
    QLabel* signatureLabel;

    QAction* addTrackAction;
    QAction* addBlockAction;
    QAction* playAction;
    QAction* stopAction;
    QLCDNumber* barLcd;
    QLCDNumber* primaryLcd;
    QLCDNumber* secondaryLcd;
    QTimer loopTimer;
    QTimer barsCounter;

    void setUpToolbar();
};


#endif //BLOCKZ_DASHBOARDVIEW_H
