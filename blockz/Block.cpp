#include "Block.h"

Block::Block(QString mediaPath, Properties properties) :
        properties{std::move(properties)},
        player{new QMediaPlayer(nullptr, QMediaPlayer::LowLatency)},
        mediaPath{std::move(mediaPath)} {
    player->setNotifyInterval(30);
    player->setMedia(QUrl::fromLocalFile(Block::mediaPath));
    player->setNotifyInterval(30);
    QObject::connect(player.get(), static_cast<void(QMediaPlayer::*)(QMediaPlayer::Error)>(&QMediaPlayer::error), this, &Block::onError);
}

bool Block::operator==(const Block& rhs) const {
    return properties == rhs.properties;
}

bool Block::operator!=(const Block& rhs) const {
    return !(rhs == *this);
}

void Block::onError(QMediaPlayer::Error error) {
    qDebug() << "Error while trying to play media: " << error;
}

const std::unique_ptr<QMediaPlayer>& Block::getPlayer() const {
    return player;
}

void Block::setPosition(const int position) {
    player->setPosition(position);
}

Properties& Block::getProperties() {
    return properties;
}

const Properties& Block::getProperties() const {
    return properties;
}

const QString& Block::getMediaPath() const {
    return mediaPath;
}

void Block::setMediaPath(const QString& mediaPath) {
    Block::mediaPath = mediaPath;
    player->setMedia(QUrl::fromLocalFile(Block::mediaPath));
}

void Block::setMute(bool muted) {
    player->setMuted(muted);
}

bool Block::isMute() {
    return player->isMuted();
}
