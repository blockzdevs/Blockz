#include "Subject.h"

void Subject::notify() {
    for (Observer* observer : observers) {
        observer->update();
    }
}

void Subject::registerObserver(Observer* observer) {
    observers.push_back(observer);
}

void Subject::removeObserver(Observer* observer) {
    observers.remove(observer);
}
